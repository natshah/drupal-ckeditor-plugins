4.16.1: ~9.2.0
4.15.0: ~9.1.0
4.14.1: ~8.9.0 || ~9.0.0
4.14.0: ~8.7.13 || ~8.8.4
4.11.3: >=8.7.0 <8.7.13 || >=8.8.0 <8.8.4
4.10.1: ~8.6.0
